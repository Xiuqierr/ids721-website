+++
title = "Home Page"
template = "index.html"
+++

# About Myself

My name is Xiuqi Chen, and I'm currently pursuing my Master's in Electrical and Computer Engineering at Duke University, where I am deeply engrossed in the expansive field of data science. My academic journey has been a fascinating exploration of how data can be harnessed to drive innovation and solve complex problems, and I am constantly seeking opportunities to further my knowledge and apply it in practical scenarios.

Outside the realm of academics, I have a vibrant array of interests that keep me energized and engaged with the world around me. I'm an avid fan of KPOP, which not only serves as a gateway to understanding different cultures but also offers a rhythmic backdrop to my daily life, fueling my creativity and passion. Anime is another passion of mine, providing a rich tapestry of stories and characters that inspire me and often offer unexpected insights into life and technology. Additionally, I love the dynamic world of football—both playing and following the sport. It teaches me the importance of teamwork, strategy, and perseverance, qualities that I find invaluable in my academic and personal pursuits.

I'm always open to connecting with like-minded individuals who share my interests in data science, music, anime, or sports. Whether it's discussing the latest breakthrough in machine learning, debating the best KPOP group, or exchanging recommendations for anime series, I believe that every conversation is an opportunity to learn something new and build lasting friendships. So, feel free to reach out if you're interested in striking up a conversation—I'm always excited to meet new people and share ideas!

<br>
<br>

# IDS-721 projects

## IDS-721 Course Description

This course is designed to give you a comprehensive view of cloud computing including Big Data, Machine Learning and Large Language Models (LLMS). A variety of learning resources will be used including interactive labs on AWS. This is a project-based course with extensive hands-on assignments and the language will be exclusively used.

## Learning Objectives

Upon successful completion of this course, you will be able to:

* Summarize the fundamentals of cloud computing
* Achieve AWS Solutions Architect Certification
* Master the Rust language for general programming
* Effectively utilize AI Pair Programming
* Evaluate the economics of cloud computing
* Accurately evaluate distributed computing challenges and opportunities and apply this knowledge to real-world projects.
* Develop non-linear life-long learning skills
* Build, share and present compelling portfolios using: GitLab, Hugging Face, YouTube, Linkedin and a personal portfolio website.
* Develop Metacognition skills (By teaching we learn)

## Core Tech Stack

Students will gain experience with an array of technologies, including:

* Rust
* AWS (AWS Learner Labs, Optional Free Tier Account)
* GitLab
* Slack
* AWS Lightsail for Research (GPU)
* CodeWhisperer


## Communication

Effective communication is facilitated through:

- Slack
- GitLab

