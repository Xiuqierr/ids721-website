# IDS721 Week 1 Project

![pipeline status](https://gitlab.oit.duke.edu/xc218/ids721-week-3-project/badges/main/pipeline.svg)

[Website Link](https://ids721-website-xiuqierr-350a6200df1e374b190c8491d70d95683e044af.gitlab.io)

This is the Zola-generated website for my IDS721 projects.


## Zola Setup
*  Initialize the project: 
    `zola init individual_website`
*  Build the project:
    `zola build`
*  Run the server:
    `zola serve`
*  Add theme
        - Download theme to `themes` directory
        - Update the `theme` variable in the configuration file

## GitLab Deployment
* Create a `.gitlab-ci.yml` file
* Configure the CI/CD Pipeline
* Commit and Push

## Hosting

This website is hosted on AWS S3.

* Select `AWS S3` to create a new bucket. make sure that the bucket is publicly accessed.
* Create a new user in IAM and get the access key and secret access key.
* Add variables `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY` to CI/CD.
* Edit the `.gitlab-ci.tml` file
```
stages:
  - deploy

default:
  image: debian:stable-slim
  tags:
    - docker

variables:
  GIT_SUBMODULE_STRATEGY: "recursive"
  ZOLA_VERSION:
    description: "The version of Zola used to build the site."
    value: "0.18.0"
  AWS_DEFAULT_REGION: "us-east-1" 
  AWS_BUCKET: "xc218-website" 

before_script:
  - apt-get update --assume-yes
  - apt-get install --assume-yes python3-pip
  - apt-get install --assume-yes awscli

pages:
  stage: deploy
  script:
    - |
    - ./zola build
    - aws s3 sync ./public s3://$AWS_BUCKET --delete
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
* Push to GitLab.

* Examine the S3 bucket:

![s3](image/s3.png)

## Website Overview
![Home Page](image/home.png)

![Project Page](image/project.png)

## Demo Video

Demo video is called `demo.mp4`, which is in the root directory.
